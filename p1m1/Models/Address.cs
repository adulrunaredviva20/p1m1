﻿using p1m1.DAL.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace p1m1.Models
{
    public class Address:Repository<Address>,IModel<Address>
    {
        [Key]
        public int? Id { get; protected set; }
        public string Street { get;  set; }
        public string District { get; set; }
        public string AreaCode { get; set; }
        public string StreetNumber { get; set; }


    

        public Address(string Street,string District, string AreaCode,string StreetNumber)
        {
            this.Street = Street;
            this.District = District;
            this.AreaCode = AreaCode;
            this.StreetNumber = StreetNumber;

        }

        public Address Get(int Id)
        {
            return this.Get(Id);
        }

        public Address() { }

        public Address Save()
        {



            this.Save(this);
            return this;
        }

        public Address Delete()
        {
            this.Delete(this);
            return this;
        }

        public Address Update()
        {
            this.Update(this);
            return this;
        }
    }
}
