﻿using p1m1.DAL.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace p1m1.Models
{
    public class Company: Repository<Company>, IModel<Company>
    {
        
        [Key]        
        public   int? Id { get; protected set; }
        public string Name { get;  set; }
        public Address Address { get;   set; }


        public Company(string Name, Address Address)
        {

            

            this.Name = Name;
            this.Address = Address;

        }

        public Company() { }

        public  Company Find(int? Id)
        {
            return this.Get(Id);
        }

        public Company Save()
        {

          
            Save(this);
            return this;
        }

        public Company Delete()
        {
             this.Delete(this);
            return this;
        }

        public Company Update()
        {
            this.Update(this);
            return this;
        }
    }
}

