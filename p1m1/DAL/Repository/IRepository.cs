﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace p1m1.DAL.Repository
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Get();
        T Get(int? Id);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        void Save(T entity);
 

        void Delete(T entity);
     

        void Update(T entity);
    }
}
