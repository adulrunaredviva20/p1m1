﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace p1m1.DAL.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        
        private readonly myDbContext context;
        public Repository(myDbContext context)
        {
            this.context = context;
        }

        public Repository()
        {
            if(context == null)
            {
                this.context = new myDbContext();
            }
        }

        public void Save(T entity)
        {

                

                context.Set<T>().Add(entity);
                context.SaveChanges();
            
        }

        public void Delete(T entity)
        {
            T existing = context.Set<T>().Find(entity);
            if (existing != null) context.Set<T>().Remove(existing);
            context.SaveChanges();
        }

        public IEnumerable<T> Get()
        {
            
            return context.Set<T>().AsEnumerable<T>();
            
        }

        

        public IEnumerable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return context.Set<T>().Where(predicate).AsEnumerable<T>();
        }

        public void Update(T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.Set<T>().Attach(entity);
            context.SaveChanges();
        }

        public T Get(int? Id)
        {
            return context.Set<T>().Find(Id);
        }
    }
}