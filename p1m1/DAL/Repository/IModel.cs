﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p1m1.DAL.Repository
{
    public interface  IModel<T>
    {

        T Save();
        T Delete();
        T Update();


    }
}
