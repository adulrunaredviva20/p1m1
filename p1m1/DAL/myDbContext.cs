﻿using Microsoft.EntityFrameworkCore;
using p1m1.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace p1m1.DAL
{
    public class myDbContext:DbContext
    {

        public DbSet<Address> Address { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Person> Person { get; set; }
        


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseInMemoryDatabase("p1m1");

               


            base.OnConfiguring(optionsBuilder);
        }

    }
}
