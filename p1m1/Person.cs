﻿using p1m1.DAL.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace p1m1.Models
{
    public class Person:Repository<Person>,IModel<Person>
    {

        
        public int? Id { get; protected set; }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }

        public Address Address { get; protected set; }

        public Person(string FirstName, string LastName, Address Address)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Address = Address;

        }

        public  Person Find(int? Id)
        {
            return this.Get(Id);
            
        }

        public Person() { }

        public Person Save()
        {

            

             this.Save(this);

            return this;
        }

        public Person Delete()
        {
            this.Delete(this);

            return this;
        }

        public Person Update()
        {
            this.Update(this);

            return this;
        }
    }
}
