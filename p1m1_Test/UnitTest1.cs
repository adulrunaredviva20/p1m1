using p1m1.DAL;
using p1m1.DAL.Repository;
using p1m1.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace p1m1_Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {

            



            var address = new Address("4875 Sun Tail", "Queen Creek", "TX", "38452");
            var person = new Person("Bill", "Smith", address);
            var company = new Company("P1M1 Software And Consulting", address);





            Assert.Null(person.Id);
            person.Save();
            Assert.NotNull(person.Id);


            Assert.Null(company.Id);
            company.Save();
            Assert.NotNull(company.Id);

            Person savedPerson = person.Find(person.Id);

            Assert.NotNull(savedPerson);
            Assert.Equal(person.Address, address);
            Assert.Equal(savedPerson.Address, address);
            Assert.Equal(person.Id, savedPerson.Id);
            Assert.Equal(person.FirstName, savedPerson.FirstName);
            Assert.Equal(person.LastName, savedPerson.LastName);
            Assert.Equal(person, savedPerson);
            Assert.Same(person, savedPerson);
            Assert.Same(person.Address, savedPerson.Address);

            Company savedCompany = company.Find(company.Id);
            Assert.NotNull(savedCompany);
            Assert.Equal(company.Address, address);
            Assert.Equal(savedCompany.Address, address);
            Assert.Equal(company.Id, savedCompany.Id);
            Assert.Equal(company.Name, savedCompany.Name);
            Assert.Equal(company, savedCompany);
            Assert.NotSame(company, savedCompany);
            Assert.NotSame(company.Address, savedCompany.Address);

            var dictionary = new Dictionary<object, object> { [address] = address, [person] = person, [company] = company };
            Assert.True(dictionary.ContainsKey(new Address("4875 Sun Tail", "Queen Creek", "TX", "38452")));
            Assert.True(dictionary.ContainsKey(new Person("Bill", "Smith", address)));
            Assert.True(dictionary.ContainsKey(new Company("P1M1 Software And Consulting", address)));
            Assert.False(dictionary.ContainsKey(new Address("54553 Apache Trail", "Queen Creek", "TX", "38452")));
            Assert.False(dictionary.ContainsKey(new Person("Jim", "Smith", address)));
            Assert.False(dictionary.ContainsKey(new Company("P1M1", address)));

            var deletedPersonId = person.Id;
            person.Delete();
            Assert.Null(person.Id);
            Assert.Null(person.Find(deletedPersonId));

            var deletedCompanyId = company.Id;
            company.Delete();
            Assert.Null(company.Id);
            Assert.Null(person.Find(deletedCompanyId));



        }
    }
}
